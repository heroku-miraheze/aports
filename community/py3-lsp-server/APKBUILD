# Contributor: Clayton Craft <clayton@craftyguy.net>
# Maintainer: Clayton Craft <clayton@craftyguy.net>
pkgname=py3-lsp-server
pkgver=1.3.2
pkgrel=0
pkgdesc="python implementation of the language server protocol, fork of python-language-server"
url="https://github.com/python-lsp/python-lsp-server"
arch="noarch !armhf"  # no py3-qt
license="MIT"
depends="py3-jedi py3-pluggy py3-ujson py3-lsp-jsonrpc py3-pydocstyle python3"
makedepends="py3-setuptools"
checkdepends="
	py3-autopep8
	py3-flake8
	py3-flaky
	py3-matplotlib
	py3-mccabe
	py3-numpy
	py3-pandas
	py3-pycodestyle
	py3-pyflakes
	py3-pylint
	py3-pytest
	py3-pytest-cov
	py3-python-versioneer
	py3-qt5
	py3-rope
	py3-yapf
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/python-lsp/python-lsp-server/archive/v$pkgver.tar.gz"
builddir="$srcdir/python-lsp-server-$pkgver"

build() {
	python3 setup.py build
}

check() {
	# deselect'ed tests are broken
	pytest -v test/ \
		--deselect test/plugins/test_flake8_lint.py \
		--deselect test/plugins/test_pylint_lint.py \
		-k "not test_jedi_completion_environment and not test_symbols_all_scopes_with_jedi_environment"
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="
d71a4c247970e34217b018faee0172a317bbf87481421fb78baf9a7079b7e4ea7d5fa76c69d2df1772d6e52ff2b8a7c20309125c87ed0aa97a88d6e741023cb0  py3-lsp-server-1.3.2.tar.gz
"
